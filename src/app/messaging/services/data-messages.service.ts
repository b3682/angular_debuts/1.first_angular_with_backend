import { MESSAGES_CONTAINER_ID } from '@angular/cdk/a11y';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UrlSegment } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { filter, map, repeat } from 'rxjs/operators';
import { Message } from '../inbox/inbox.component';

@Injectable({
  providedIn: 'root'
})
export class DataMessagesService {

  private messages: Message[]=[

  ];

  private subject = new BehaviorSubject<Message[]>([]);

  constructor(
    private http : HttpClient
  )
  {
    this.subject.getValue()


  }
  getMessages(): Observable<Message[]> {


    return this.http.get<Message[]>('http://localhost:3000/messages');
    // .pipe()
    // si ça se passe bien lors de la recup de nos datas
    // requete asynchrone -> on va return en premier ce qui passe après la requete get au server
    // .subscribe(response => {
    //   ({data :  response});


    // },
    // // si echec de recup datas
    // error => {

    // },

    // // complete
    // () => {

    // },
    // )

    // // lors de l'execution ci dessus on return
    // // si on veut recup une reponse apres le get (requete asynchrone) -> on utilise un callback
    // return this.subject.getValue();
    // // console.log(this.subject.getValue());
  }

  addMessages( message : Message) {

    //extraction de la valeur de nos messages
    const msgs = this.subject.getValue();
    // on push dans nos message, notre nouveau message
    msgs.push(message);
    this.subject.next(msgs);

    this.messages.push(message);
    console.log(this.messages);

  }

  //methode observable qui va nous permettre de modifier les messages (qu'on type avec ce qu'on veut manipuler)
  getObeservableMessages() : Observable<Message[]>{
    return of(this.messages);
  }

  // on veut retourner un observable, s'abonner à l'observable
  getSubjectMessages(): Observable<Message[]> {
    // on fait ça pour retourner uniqument les méthodes de l'observable et non pas toutes celles de subject
    return this.subject.asObservable();
  }

  getUnreadMessagesAsObservable(): Observable<Message[]>{
    //liste des messages dans subject (this.subject pointe sur Message[])
    return this.subject
      // pipe devient un observable qui recoit la liste
      .pipe(
        //map va return toute la liste des messages non lus grâce à => filter qui va retourner une liste -> de messages isRead: false
        map(msgs => msgs.filter(m => !m.isRead)),
        // repeat() //ça boucle toutes les secondes pour mettre à jour -> pas opti
      )
  }

  getCountOfUnreadMessages(): Observable<Number> {
    return this.subject
    .pipe(
      // on recup tous les msgs unread
      map(msgs => msgs.filter(m => !m.isRead)),
      // on passe dans urm la liste des messages unread, et on les comptes !
      map(urm => urm.length)
    )
  }


}
