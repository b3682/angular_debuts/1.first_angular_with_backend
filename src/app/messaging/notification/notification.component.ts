import { Component, OnInit } from '@angular/core';
import { DataMessagesService } from '../services/data-messages.service';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  notReadMessages: Number;

  constructor(
    private service: DataMessagesService
  ) { }

  ngOnInit(): void {

    this.service
    .getCountOfUnreadMessages()
    .subscribe( n => this.notReadMessages = n)

    // // retourner le nombre de unread messages
    // this.service
    // //retourne un observable
    // .getSubjectMessages()
    // //traitement intermédiaire avec le pipe
    // .pipe(
    //   map((msgs,idx) =>  msgs.filter(m =>  !m.isRead).length)
    // )
    // .subscribe(datas => this.notReadMessages = datas);
    //   // filter itère sur chaque message et prend un etat en argument pour filtrer ce de cet etat etc
  }

  set($event) {

  }

}
