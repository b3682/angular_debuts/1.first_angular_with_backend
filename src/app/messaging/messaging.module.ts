import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationComponent } from './notification/notification.component';
import { InboxComponent } from './inbox/inbox.component';
import { FormsModule } from '@angular/forms';
import { WritingComponent } from './writing/writing.component';
import { MessageComponent } from './message/message.component';



@NgModule({
  declarations: [
    NotificationComponent,
    InboxComponent,
    WritingComponent,
    MessageComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports:[
    NotificationComponent,
    InboxComponent
  ]
})
export class MessagingModule { }
