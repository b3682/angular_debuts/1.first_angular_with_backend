import { style } from '@angular/animations';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DataMessagesService } from '../services/data-messages.service';



// ici on va typer la structure de nos msg
export interface Message {
  title: string;
  badge?: number;
  content: string;
  sent?: Date;
  isRead: boolean;

}


@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})

//on va structurer le contenu de nos messages du component inbox
export class InboxComponent implements OnInit {

  @Input() content = "contenu de du component";

  @Output() dataevent = new EventEmitter();


  bntStyle: string;
  // un message avec son titre et l'interface qu'on a implémenté au dessus
  title: string = 'Message';
  m: Message = {
    title: 'titre du message',
    content: 'contenu du msg',
    isRead: false,
    sent: new Date()
  }


  // des messages dans un tableau avec un titre et la structure implémenté du dessus
  title2: string = 'Messages';

  messages: Message[] = [
    // recup les msg de dataservices
  ]

  messageRead = 'message-unread';

  // recup le nombre de message
  badge: number = this.messages.length;


  constructor(
    private myService: DataMessagesService
  ) {

  }

  ngOnInit(): void {

    // on recup notre méthode getMessages() qui provient dataservice messages,
    // on s'abonne à nos datas du backend (nos messages), et on les passes dans this.messages pour pouvoir y accéder en html
    this.myService.getMessages()
    .subscribe((datas) => {
      this.messages = datas;
    })

    //subscribe = à chaque changement effectué dans message --> demande un callback de getobservable situé dans services
    // datas provient du typage dans la fonction de getObservable
    // en gros on s'abonne aux messages pour savoir si un est modifié ou non, et si oui, on met à jour les messages
    // this.myService.getObeservableMessages()
    // .subscribe((datas) => {
    //   this.messages = datas;
    // });

  }

  // méthodes >>>>>>>>>>>>>>







  // mettre a jour badge(nb msg) on click
  notifs() {
    this.badge = this.messages.length;
    // console.log(this.messages.length);
  }

  remove(event : any) {
    // console.log(event);
    const index: number = this.messages.indexOf(event);
    //this.messages.remove()
    // console.log(event);
    // console.log(index);
    this.messages.splice(index, 1);
  }

  read(event : any) {
    // console.log("test");

  }
}
