import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationComponent } from '../notification/notification.component';
import { WritingComponent } from '../writing/writing.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  exports:[
  NotificationComponent,
  WritingComponent
  ]
})
export class InboxModule { }
