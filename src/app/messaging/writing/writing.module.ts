import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InboxComponent } from '../inbox/inbox.component';
import { InboxModule } from '../inbox/inbox.module';
import { WritingComponent } from './writing.component';
import { DataMessagesService } from '../services/data-messages.service';



@NgModule({
  providers: [
    DataMessagesService
  ],
  declarations: [],
  imports: [
    CommonModule,
    InboxModule,
  ],
  exports: [
    InboxComponent,
    WritingComponent
  ]
})
export class WritingModule { }
