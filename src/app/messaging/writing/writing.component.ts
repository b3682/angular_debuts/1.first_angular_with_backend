import { Component, OnInit } from '@angular/core';
import { Message } from '../inbox/inbox.component';
import { DataMessagesService } from '../services/data-messages.service';

export interface sentMessage {
  sentContent: string;
}



@Component({
  selector: 'app-writing',
  templateUrl: './writing.component.html',
  styleUrls: ['./writing.component.scss'],
  providers: [ DataMessagesService ]
})
export class WritingComponent implements OnInit {

  m:Message;
  constructor(
    private service : DataMessagesService

  ) { }

  ngOnInit(): void {
    this.m = {
      title: '',
      content: '',
      isRead: false,
      sent: new Date()
    }
  }





  get_content() {
    // this.sm.sentContent = this.m.content
    // console.log(this.m.content);
    this.service.addMessages(this.m)
    console.log(this.service.getMessages());


  }
}
