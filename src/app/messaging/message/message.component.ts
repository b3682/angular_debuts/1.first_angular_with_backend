import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Message } from '../inbox/inbox.component';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  @Input('data') message: any;
  @Output() delete = new EventEmitter();
  @Output() read = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }
  deleteMsg() {
    this.delete.emit(this.message)
  }
  readMsg() {
    this.read.emit(this.message)
  }
}
