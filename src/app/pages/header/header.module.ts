import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbAccordionComponent, NbActionComponent, NbActionsModule } from '@nebular/theme';



@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    NbActionsModule

  ]
})
export class HeaderModule { }
