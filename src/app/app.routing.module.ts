import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { InboxComponent } from "./messaging/inbox/inbox.component";
import { NotificationComponent } from "./messaging/notification/notification.component";
import { WritingComponent } from "./messaging/writing/writing.component";
import { ProfileComponent } from "./pages/profile/profile.component";


@NgModule({
  imports:[
    RouterModule.forRoot([
        {path: 'notification',component:NotificationComponent},
        {path: 'inbox', component:InboxComponent},
        {path: 'profile', component:ProfileComponent},
        {path: 'writing', component:WritingComponent}
    ]),
  ],
  exports:[
    RouterModule,
  ]

})

export class RoutingModule {

}
